#output "instances" {
#  value       = "${aws_instance.rhel9}"
#  description = "EC2 details"
#}

output "IP_Address" {
  value       = "${aws_instance.rhel9.public_ip}"
  description = "EC2 I.P address"
}

output "URL" {
  value       = "http://${aws_instance.rhel9.public_dns}"
  description = "EC2 DNS name"
}
