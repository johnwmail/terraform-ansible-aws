variable "ssh_username" {
  description = "the ssh user to use"
  default     = "ec2-user"
}

variable "ssh_public_key" {
  description = "the public key to use"
  default     = "~/.ssh/aws.id_rsa.pub"
}

variable "ssh_private_key" {
  description = "the private key to use"
  default     = "~/.ssh/aws.id_rsa"
}
