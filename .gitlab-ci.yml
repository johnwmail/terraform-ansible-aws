# Pipeline will only trigger when something is pushed to $default branch 
workflow:  
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH || $AWS_ACCESS_KEY_ID == null || $AWS_SECRET_ACCESS_KEY == null
      when: never
    - when: always

# List of variables
variables:
  TF_DIR: ${CI_PROJECT_DIR}       # Directory in which terraform files are kept
  STATE_NAME: "default"          # Name of terraform state used in "terraform init"command
  ADDRESS: "${CI_SERVER_URL}/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${STATE_NAME}"

  # List of secret variables
  # AWS_ACCESS_KEY_ID (aws login keyid)
  # AWS_SECRET_ACCESS_KEY (aws login token)
  # GITLAB_USER (gitlab login user)
  # GITLAB_ACCESS_TOKEN (gitlab login token)
  # SSH_PRIVATE_KEY (File type)
  # SSH_PUBLIC_KEY (File type)

# Stages of the pipeline
stages:
  - validate
  - plan
  - apply
  - destroy

# Image which will use in each stage
image:
  name: registry.gitlab.com/johnwmail/alpine-terraform-ansible:latest

# Script to be executed before each stage 
before_script:
  - ansible --version
  - ansible-galaxy collection list
  - terraform --version

  - cd ${TF_DIR}            # To get inside the working directory
  # To initiate terraform backend / gitlab managed terraform state
  - |
    terraform init -reconfigure \
      -backend-config="address=${ADDRESS}" \
      -backend-config="lock_address=${ADDRESS}/lock" \
      -backend-config="unlock_address=${ADDRESS}/lock" \
      -backend-config="username=$GITLAB_USER" \
      -backend-config="password=$GITLAB_ACCESS_TOKEN" \
      -backend-config="lock_method=POST" \
      -backend-config="unlock_method=DELETE" \
      -backend-config="retry_wait_min=5"

  # To initiate ssh setup
  - mkdir -m 700 ~/.ssh
  - touch ~/.ssh/known_hosts
  - echo "Host *" > ~/.ssh/config
  - echo "  StrictHostKeyChecking=no" >> ~/.ssh/config
  - echo "  UserKnownHostsFile=/dev/null" >> ~/.ssh/config
  - cp "$SSH_PRIVATE_KEY" ~/.ssh/aws.id_rsa
  - cp "$SSH_PUBLIC_KEY" ~/.ssh/aws.id_rsa.pub
  - chmod 600 ~/.ssh/*
  
# To validate terraform files configuration
validate:
  stage: validate
  script:
    - terraform validate

# To check the plan of the infrastructure
plan:
  stage: plan
  script:
    - terraform plan 
  dependencies:              
    - validate

# To create infrastructure on AWS
apply:
  stage: apply
  script:
    - terraform apply  -auto-approve
  dependencies:              
    - plan

# To destroy infrastructure on cloud. It needs manual approval 
destroy:
  stage: destroy
  script:
    - terraform destroy  -auto-approve
  dependencies:          
    - plan
    - apply
  when: manual

