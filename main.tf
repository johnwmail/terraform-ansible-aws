provider "aws" {
  #region  = "ap-southeast-1"
  region  = "ap-east-1"
}

resource "aws_key_pair" "sshkey" {
  key_name = "terraform-aws-key"
  public_key = file("${var.ssh_public_key}")
}

resource "aws_instance" "rhel9" {
  #ami           = "ami-09b1e8fc6368b8a3a"
  ami           = "ami-0951cee7b6ae09843"
  #instance_type = "t2.micro"
  instance_type = "t3.micro"

  credit_specification {
    cpu_credits = "standard"
  }

  key_name      = aws_key_pair.sshkey.key_name

  get_password_data                    = "false"
  ipv6_address_count                   = "0"

  root_block_device {
    delete_on_termination = "true"
    encrypted             = "false"
    volume_size           = "30"
    volume_type           = "gp3"
  }

  connection {
    type        = "ssh"
    user        = "${var.ssh_username}"
    private_key = file("${var.ssh_private_key}")
    host        = aws_instance.rhel9.public_ip
  }

  # don't know why must be connected it first, otherwise ansible_playbook not working.
  provisioner "remote-exec" {
    inline = [
      "touch hello.txt",
      "echo 'Hello World'",
      "echo 'Have a great day!' >> hello.txt"
    ]
  }

#  # copy file to remote
#  provisioner "file" {
#    source      = "./local-file"
#    destination = "./remote-file"
#  }
  vpc_security_group_ids = [aws_security_group.basic_web.id]
}

data "aws_vpc" "default" {
  default = true
}

resource "aws_security_group" "basic_web" {
  name        = "basic_web"
  description = "Allow HTTP/HTTPS to web server"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description = "SSH ingress"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    # gitlab share runner on (GCP) in us-east1
    cidr_blocks = ["34.23.0.0/16","34.24.0.0/15","34.26.0.0/16","34.73.0.0/16","34.74.0.0/15","34.98.128.0/21","34.118.250.0/23","34.138.0.0/15","34.148.0.0/16","34.152.72.0/21","34.177.40.0/21","35.185.0.0/17","35.190.128.0/18","35.196.0.0/16","35.207.0.0/18","35.211.0.0/16","35.220.0.0/20","35.227.0.0/17","35.229.16.0/20","35.229.32.0/19","35.229.64.0/18","35.231.0.0/16","35.237.0.0/16","35.242.0.0/20","35.243.128.0/17","104.196.0.0/18","104.196.65.0/24","104.196.66.0/23","104.196.68.0/22","183.179.12.119/32","129.150.47.17/32","152.69.213.176/32","138.2.66.215/32","104.196.96.0/19","104.196.128.0/18","104.196.192.0/19","162.216.148.0/22"]
  }

  ingress {
    description = "HTTP ingress"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS ingress"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
   from_port   = 0
   to_port     = 0
   protocol    = "-1"
   cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "terraform-basic_web-security-group"
  }
}
