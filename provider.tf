terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
    ansible = {
      version = "~> 1.2.0"
      source  = "ansible/ansible"
    }
    null = {
      version = "~> 3.2.2"
      source  = "hashicorp/null"
    }
  }
  backend "http" {}
}
