resource "ansible_playbook" "playbook" {
  playbook   = "playbook/main.yml"
  name       = "${aws_instance.rhel9.public_ip}"
  replayable = true
  #verbosity  = 3
  ignore_playbook_failure = true

  extra_vars = {
    ansible_user = "${var.ssh_username}"
    ansible_ssh_private_key_file = "${var.ssh_private_key}"
    ansible_ssh_common_args = "'-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'"
  }
}

